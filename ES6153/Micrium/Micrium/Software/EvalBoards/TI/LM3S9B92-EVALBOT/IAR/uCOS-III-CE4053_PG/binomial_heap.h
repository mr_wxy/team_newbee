#include <os.h>
#include <includes.h>
#ifndef _BINOMIAL_HEAP_H_
#define _BINOMIAL_HEAP_H_

typedef int Type;

typedef struct _BinomialNode{
    Type   key;                       
    OS_TCB *tcb;
    int degree;
    int period;
    struct _BinomialNode *child;   
    struct _BinomialNode *parent;    
    struct _BinomialNode *next;        
}BinomialNode, *BinomialHeap;


BinomialNode* binomial_insert(BinomialHeap heap, Type key,int period,OS_TCB *tcb);

BinomialNode* binomial_delete(BinomialHeap heap, Type key);

void binomial_update(BinomialHeap heap, Type oldkey, Type newkey);


BinomialNode* binomial_union(BinomialHeap h1, BinomialHeap h2) ;


BinomialNode* binomial_search(BinomialHeap heap, Type key);

BinomialNode* binomial_minimum(BinomialHeap heap) ;

BinomialNode* binomial_extract_minimum(BinomialHeap heap);


void binomial_print(BinomialHeap heap);

#endif