#include <os.h>
#include <includes.h>
#ifndef _AVL_TREE_H_  
#define _AVL_TREE_H_  
  
typedef int Type;  
  
typedef struct AVLTreeNode{  
    OS_TCB *tcb;
    int period;
    Type key;                    // key  
    int height;  
    struct AVLTreeNode *left;     
    struct AVLTreeNode *right;     
}Node, *AVLTree;  
  
// get the height  
int avltree_height(AVLTree tree);  
  
// preorder"  
void preorder_avltree(AVLTree tree);  
// inorder  
void inorder_avltree(AVLTree tree,OS_TCB *tcb);  
// postorder 
void postorder_avltree(AVLTree tree);  
  
void print_avltree(AVLTree tree, Type key, int direction);  
  
// return the node witch has the key (recursive)
Node* avltree_search(AVLTree x, Type key);  
// return the node witch has the key (unrecursive) 
Node* iterative_avltree_search(AVLTree x, Type key);  
  
// find the minimum node   
Node* avltree_minimum(AVLTree tree);  
// find the maxmum node    
Node* avltree_maximum(AVLTree tree);  
  
// insert into AVL tree, return root node 
Node* avltree_insert(AVLTree tree, Type key,int period,OS_TCB *tcb);  
  
// delete the node with the key 
Node* avltree_delete(AVLTree tree, Type key);  
  
// destory 
void destroy_avltree(AVLTree tree);  
  
  
#endif  