#include <os.h>


// create task function
void          OSRecTaskCreate              (OS_TCB                *p_tcb,
                                         CPU_CHAR              *p_name,
                                         OS_TASK_PTR            p_task,
                                         void                  *p_arg,
                                         OS_PRIO                prio,
                                         CPU_STK               *p_stk_base,
                                         CPU_STK_SIZE           stk_limit,
                                         CPU_STK_SIZE           stk_size,
                                         OS_MSG_QTY             q_size,
                                         OS_TICK                time_quanta,
                                         void                  *p_ext,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
// delete task function
#if OS_CFG_TASK_DEL_EN > 0u
void          OSRecTaskDel                 (OS_TCB                *p_tcb,
                                         OS_ERR                *p_err);
#endif
                                         typedef int Type;
//tasks  action  functions                              
void moveForward();
void moveBackward();
void leftLEDBlink();
void rightLEDBlink();
void LEDBlink();
//task functions
static  void  AppTask1 (void  *p_arg);
static  void  AppTask2 (void  *p_arg);
static  void  AppTask3 (void  *p_arg);
static  void  AppTask4 (void  *p_arg);
static  void  AppTask5 (void  *p_arg);
// find the task by check its period
void SwitchTask(int i);
// task scheduling function
void TaskScheduling();
// task recurstion function
void TaskRecursion(int SecCount);
// initiate the data structure
void StructInitial();
// return the root of the AVLtree
AVLTree *RetrunAVLTree();
                                         


                                         