/*
************************************************************************************************************************
*                                                      uC/OS-III
*                                                 The Real-Time Kernel
*
*                                  (c) Copyright 2009-2011; Micrium, Inc.; Weston, FL
*                           All rights reserved.  Protected by international copyright laws.
*
*                                                   TASK MANAGEMENT
*
* File    : OS_TASK.Cs
* By      : JJL
* Version : V3.02.00
*
* LICENSING TERMS:
* ---------------
*           uC/OS-III is provided in source form for FREE short-term evaluation, for educational use or 
*           for peaceful research.  If you plan or intend to use uC/OS-III in a commercial application/
*           product then, you need to contact Micrium to properly license uC/OS-III for its use in your 
*           application/product.   We provide ALL the source code for your convenience and to help you 
*           experience uC/OS-III.  The fact that the source is provided does NOT mean that you can use 
*           it commercially without paying a licensing fee.
*
*           Knowledge of the source code may NOT be used to develop a similar product.
*
*           Please help us continue to provide the embedded community with the finest software available.
*           Your honesty is greatly appreciated.
*
*           You can contact us at www.micrium.com, or by phone at +1 (954) 217-2036.
************************************************************************************************************************
*/
#include <includes.h>
#include <OS_RecTask.h>
#include <binomial_heap.h>
#include <AVLTree.h>
#include <234tree.h>
#include <slpayTree.h>

#define ONESECONDTICK             7000000

#define TASK1PERIOD                   10
#define TASK2PERIOD                   20


#define WORKLOAD1                     2
#define WORKLOAD2                     2


#define TIMERDIV                      (BSP_CPUClkFreq() / (CPU_INT32U)OSCfg_TickRate_Hz

#define APP_TASK_PRIO                 1u

CPU_TS32 dispatchOverhead; 
CPU_TS32 schedulingOverhead;
CPU_TS32 mutexAcquire;
CPU_TS32 mutexRelease;
CPU_TS32 BlockInsertion;
CPU_TS32 BlockIRelease;
CPU_TS32 executionTime;

CPU_TS32 *EX(){
  return &executionTime;
}
CPU_TS32 interruptLantency;

CPU_TS32 *IL(){
  return &interruptLantency;
}


CPU_TS32 *DO(){
  return &dispatchOverhead;
}
CPU_TS32 *SO(){
  return &schedulingOverhead;
}

CPU_TS32 *MA(){
  return &mutexAcquire;
}

CPU_TS32 *MR(){
  return &mutexRelease;
}

CPU_TS32 *BI(){
  return &BlockInsertion;
}
CPU_TS32 *BR(){
  return &BlockIRelease;
}
//OS_TS_GET();
BinomialHeap recursionHeap=NULL;
AVLTree shceduleTree=NULL;
t34_node *blockList=NULL;
tree_node  *sharingResource=NULL;

int systemCelling=100;

int returnSC(){
  return systemCelling;
}

void setSC(int i){
  systemCelling=i;
}

BinomialNode* MinimumRelease=NULL;
Node* HighestPriority=NULL;
int BasicPriority=2;
int TmpPeriod;
int firstCome=1;

OS_MEM        BinomialBuffer;
OS_MEM        AVLTreeBuffer;
OS_MEM        Two34TreeBuffer;
OS_MEM        SlpayTreeBuffer;
BinomialNode BinomialPart[6][sizeof(BinomialNode)];
Node AVLTreePart[32][sizeof(Node)];
t34_node Two34TreePart[6][sizeof(t34_node)];
tree_node SlpayTreePart[6][sizeof(tree_node)];


int NonPriorityTask=100;

int TickCount=0;
int SecCount=0;

int TaskName=0;


int *GetNonPriorityTask(){
  return &NonPriorityTask;
}
int SetNonPriorityTask(int i){
  *GetNonPriorityTask()=i;
}

OS_MEM *ReturnBinomialBuffer(){
  return &BinomialBuffer;
}
OS_MEM *ReturnAVLTreeBuffer(){
  return &AVLTreeBuffer;
}
OS_MEM *ReturnTwo34TreeBuffer(){
  return &Two34TreeBuffer;
}
OS_MEM *ReturnSlpayTreeBuffer(){
  return &SlpayTreeBuffer;
}


BinomialHeap *returnHeap()
{
  return &recursionHeap;
}
AVLTree *returnTree(){
  return &shceduleTree;
}
t34_node **returnBlock(){
  return &blockList;
}
tree_node **returnResource(){
  return &sharingResource;
}

int FirstCome(){
  return firstCome;
}
void AfterFirstCome(){
  firstCome=0;
}
void BeforeFirstCome(){
  firstCome=1;
}

void moveForward()
{
   CPU_INT32U  k, i, j;
   RoboTurn(FRONT, 16, 50);
   for(k=0; k<2; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
        j=2*i;
      }
     }
    BSP_MotorStop(LEFT_SIDE);
    BSP_MotorStop(RIGHT_SIDE);
}

void moveBackward()
{
   CPU_INT32U  k, i, j;
   RoboTurn(BACK, 16, 50);
   for(k=0; k<2; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
        j=2*i;
      }
     }
    BSP_MotorStop(LEFT_SIDE);
    BSP_MotorStop(RIGHT_SIDE);
}



void leftLEDBlink()
{
    CPU_INT32U  i,j=0;
    BSP_LED_Off(0u);
    BSP_LED_Toggle(2u);
    for(i=0; i <ONESECONDTICK/2; i++)
        j = ((i * 2)+j);
    BSP_LED_Off(0u);
}

void rightLEDBlink()
{
   CPU_INT32U  i,j=0;
    BSP_LED_Off(0u);
    BSP_LED_Toggle(1u);
    for(i=0; i <ONESECONDTICK/2; i++)
        j = ((i * 2)+j);
    BSP_LED_Off(0u);
}
void LEDBlink()
{
    CPU_INT32U  i,j=0;
    BSP_LED_Off(0u);
    BSP_LED_Toggle(0u);
    for(i=0; i <ONESECONDTICK/2; i++)
        j = ((i * 2)+j);
    BSP_LED_Off(0u);
}



int arg1=123;
int startRunning=0;
int isRuning=0;
int *GetIsRuning()
{
  return &isRuning;
}
int *GetStartRunning()
{
  return &startRunning;
}

int isStart=0;
int canScheduling=0;
int taskNum=0;
int waitingTaskNum=0;
CPU_TS OverheadstartTime=0;
CPU_TS OverheadTime=0;
void TaskScheduling()
{
  *(CPU_TS32*)SO()=OS_TS_GET();
  HighestPriority=avltree_minimum(shceduleTree);
  while(HighestPriority!=NULL){
        OverheadTime=OS_TS_GET()-OverheadstartTime;
  int tempKey=HighestPriority->key;
  TmpPeriod=HighestPriority->period;
  OS_TCB *tcb=HighestPriority->tcb;
  if(OSTCBCurPtr==tcb){
    shceduleTree=avltree_delete(shceduleTree,tempKey);
    HighestPriority=avltree_minimum(shceduleTree);
    shceduleTree=avltree_insert(shceduleTree,tempKey,tempKey,tcb);
  }else
  {
  shceduleTree=avltree_delete(shceduleTree,tempKey);
  HighestPriority=avltree_minimum(shceduleTree);
  OS_ERR      err;
  *(CPU_TS32*)SO()=OS_TS_GET()-*(CPU_TS32*)SO();
  //waitingTaskNum--;
  OSTaskCreate((OS_TCB     *)tcb,
                  (CPU_CHAR   *)tcb->NamePtr, 
                  (OS_TASK_PTR ) tcb->TaskEntryAddr, 
                   (void *)&arg1, 
                  (OS_PRIO     ) tcb->Prio, 
                  (CPU_STK    *) tcb->StkBasePtr, 
                  (CPU_STK_SIZE) tcb->StkSize / 10u, 
                  (CPU_STK_SIZE) tcb->StkSize, 
                  (OS_MSG_QTY  ) 0u, 
                  (OS_TICK     ) 0u, 
                  (void       *)(CPU_INT32U) 1, 
                  (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err);
    }
  }
}

int *GetTaskNum(){
  return &taskNum;
}

void TaskRecursion(int SecCount)
{
  *(CPU_TS32*)DO()=OS_TS_GET();
  MinimumRelease=binomial_minimum(recursionHeap);
  if(MinimumRelease->key>SecCount/1500){
    return;
  }
  //
  while(MinimumRelease->key<=SecCount/1500)
  {
    
    //our team is using RM sheduling, so task with shorter period has higer priority
    //shceduleTree=avltree_insert(shceduleTree,BasicPriority+MinimumRelease->period,MinimumRelease->period,MinimumRelease->tcb);
    OS_TCB *tcb=MinimumRelease->tcb;
    int temp=MinimumRelease->key;
    int period=MinimumRelease->period;
    recursionHeap=binomial_insert(recursionHeap,(temp+period),period,MinimumRelease->tcb);
    recursionHeap=binomial_delete(recursionHeap,MinimumRelease->key);
    //binomial_update_key(recursionHeap,MinimumRelease,(temp+MinimumRelease->period));
    MinimumRelease=binomial_minimum(recursionHeap);
    *(int *)GetI()=0;
    inorder_avltree(shceduleTree,tcb);
    if(*(int *)GetI()!=1){
    shceduleTree=avltree_insert(shceduleTree,period,period,tcb);
    }
    *(int *)GetI()=0;
    startRunning=1;
    /*OS_ERR      err;
    OSTaskCreate((OS_TCB     *)tcb,
                  (CPU_CHAR   *)tcb->NamePtr, 
                  (OS_TASK_PTR ) tcb->TaskEntryAddr, 
                   (void *)&arg1, 
                  (OS_PRIO     ) tcb->Prio, 
                  (CPU_STK    *) tcb->StkBasePtr, 
                  (CPU_STK_SIZE) tcb->StkSize / 10u, 
                  (CPU_STK_SIZE) tcb->StkSize, 
                  (OS_MSG_QTY  ) 0u, 
                  (OS_TICK     ) 0u, 
                  (void       *)(CPU_INT32U) 1, 
                  (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err);*/
  }
  *(CPU_TS32*)DO()=OS_TS_GET()-*(CPU_TS32*)DO();
  TaskScheduling();
}

void MemInit(){
    /*
    recursionHeap=binomial_insert(recursionHeap,5,5);
    recursionHeap=binomial_insert(recursionHeap,15,15);
    recursionHeap=binomial_insert(recursionHeap,20,20);
    recursionHeap=binomial_insert(recursionHeap,35,35);
    recursionHeap=binomial_insert(recursionHeap,60,60);
  */
    /*
    shceduleTree=avltree_insert(shceduleTree,BasicPriority,5);
    shceduleTree=avltree_insert(shceduleTree,BasicPriority+1,15);
    shceduleTree=avltree_insert(shceduleTree,BasicPriority+2,20);
    shceduleTree=avltree_insert(shceduleTree,BasicPriority+3,35);
    shceduleTree=avltree_insert(shceduleTree,BasicPriority+4,60);
  */
  // create the memory space for data structure
    OS_ERR  err;
    OSMemCreate((OS_MEM  *)&BinomialBuffer,
                (CPU_CHAR *)"BinomialBuffer",
                (void  *)&BinomialPart[0][0],
                (OS_MEM_QTY )6,
                (OS_MEM_SIZE)sizeof(BinomialNode),
                (OS_ERR     *)&err);
    OSMemCreate((OS_MEM  *)&AVLTreeBuffer,
                (CPU_CHAR *)"AVLTreeBuffer",
                (void  *)&AVLTreePart[0][0],
                (OS_MEM_QTY )32,
                (OS_MEM_SIZE)sizeof(BinomialNode),
                (OS_ERR     *)&err);
    OSMemCreate((OS_MEM  *)&Two34TreeBuffer,
                (CPU_CHAR *)"Two34TreeBuffer",
                (void  *)&Two34TreePart[0][0],
                (OS_MEM_QTY )6,
                (OS_MEM_SIZE)sizeof(t34_node),
                (OS_ERR     *)&err);
    OSMemCreate((OS_MEM  *)&SlpayTreeBuffer,
                (CPU_CHAR *)"SlpayTreeBuffer",
                (void  *)&SlpayTreePart[0][0],
                (OS_MEM_QTY )6,
                (OS_MEM_SIZE)sizeof(tree_node),
                (OS_ERR     *)&err);
}

AVLTree RetrunAVLTree(){
  return shceduleTree;
}
//timer
void OSTimeTickHook()
{
  OverheadstartTime=OS_TS_GET();
  *(CPU_TS32*)IL()=OS_TS_GET();
  TickCount++;
  //if overflow after 300 s reset the counter
  if(TickCount>=90000){
    // reset the all node's period in recursion list
    binomial_print(recursionHeap);
    TickCount=0;
  }
  TaskRecursion(TickCount);
  /*
  if(isRuning!=1&&startRunning==1&&shceduleTree!=NULL){
      isRuning=1;
      TaskScheduling();
  }
  */
  //every 1/1000 s
  *(CPU_TS32*)IL()=OS_TS_GET()-*(CPU_TS32*)IL();
  int i=0;
}

void  OSIdleTaskHook (void){
  /*
  if(shceduleTree!=NULL){
    TaskScheduling();
  }
  */
}
