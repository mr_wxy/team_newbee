#include <os.h>
#include <includes.h>

typedef struct two34TreeNode{
    OS_TCB      *p_tcb[3];
    CPU_INT32U  prio[3];
    struct two34TreeNode *child[4];
    CPU_INT32U  keynum;
    CPU_INT32U isLeaf;
}TwoThreeFourNode, *TwoThreeFourTree;
void BTree_split_child(TwoThreeFourNode* parent, int index, TwoThreeFourNode* node);
void BTree_insert_nonfull(TwoThreeFourNode* node, OS_TCB *p_tcb, CPU_INT32U prio);
void BTree_insert(TwoThreeFourTree* tree, OS_TCB *p_tcb, CPU_INT32U prio);
void BTree_merge_child(TwoThreeFourTree* tree, TwoThreeFourNode* node, CPU_INT32U index);
void BTree_recursive_remove(TwoThreeFourTree* tree, OS_TCB *p_tcb, CPU_INT32U prio);
void BTree_remove(TwoThreeFourTree* tree, OS_TCB *p_tcb, CPU_INT32U prio) ;