#include <stdio.h>
#include <stdlib.h>
#include "234tree.h"

void BTree_split_child(TwoThreeFourNode* parent, int index, TwoThreeFourNode* node)  
{
    OS_ERR err;
    CPU_INT32U i;  
    TwoThreeFourNode* newNode;  
  
    newNode = (TwoThreeFourNode*)OSMemGet((OS_MEM *)ReturnTwo34TreeBuffer(),
                                  &err);
    
    if (!newNode) {    
        return;  
    }  
  
    newNode->isLeaf = node->isLeaf;  
    newNode->keynum = 1;  
   
    for (i = 0; i < newNode->keynum; ++i){  
        newNode->prio[i] = node->prio[2 + i];
        newNode->p_tcb[i] = node->p_tcb[2 + i];
        node->prio[2 + i] = 0;
        node->p_tcb[2 + i] = NULL;
    }  
  

    if (node->isLeaf == 0) {  
        for (i = 0; i < 2; i++) {  
            newNode->child[i] = node->child[2 + i];  
            node->child[2 + i] = NULL;  
        }  
    }  
  
    node->keynum = 1;  
   
    for (i = parent->keynum; i > index; --i) {  
        parent->child[i + 1] = parent->child[i];  
    }  
  
    parent->child[index + 1] = newNode;  
  
    for (i = parent->keynum - 1; i >= index; --i) {  
        parent->prio[i + 1] = parent->prio[i];
        parent->p_tcb[i + 1] = parent->p_tcb[i];
    }  
  
    parent->prio[index] = node->prio[1];
    parent->p_tcb[index] = node->p_tcb[1];
    ++parent->keynum;  
  
    node->prio[1] = 0;
    node->p_tcb[1] = NULL;
}

void BTree_insert_nonfull(TwoThreeFourNode* node, OS_TCB *p_tcb, CPU_INT32U prio)  
{  
    CPU_INT32U i;  
  
    if (node->isLeaf == 1) {  
        i = node->keynum - 1;  
        while (i >= 0 && prio < node->prio[i]) {  
            node->prio[i + 1] = node->prio[i];
            node->p_tcb[i + 1] = node->p_tcb[i];
            --i;  
        }
  
        node->prio[i + 1] = prio;
        node->p_tcb[i + 1] = p_tcb;
        
        ++node->keynum;  
    }  
   
    else {  
        i = node->keynum - 1;  
        while (i >= 0 && prio < node->prio[i]) {  
            --i;  
        }  
  
        ++i;  
  
        if (node->child[i]->keynum == 3) {  
            BTree_split_child(node, i, node->child[i]);  
            if (prio > node->prio[i]) {  
                ++i;  
            }  
        }  
        BTree_insert_nonfull(node->child[i], p_tcb, prio);  
    } 
}


void BTree_insert(TwoThreeFourTree* tree, OS_TCB *p_tcb, CPU_INT32U prio)  
{  
    OS_ERR err;
    TwoThreeFourNode* node;  
    TwoThreeFourNode* root = *tree;  
  
    if (NULL == root) {
        root = (TwoThreeFourNode*)OSMemGet((OS_MEM *)ReturnTwo34TreeBuffer(),
                                  &err); 
        if (!root) {  
            return;  
        }  
        root->isLeaf = 1;  
        root->keynum = 1;  
        root->prio[0] = prio;
        root->p_tcb[0] = p_tcb;

        *tree = root;  
  
        return;  
    }  
   
    if (root->keynum == 3) {
        node = (TwoThreeFourNode*)OSMemGet((OS_MEM *)ReturnTwo34TreeBuffer(),
                                  &err);
        if (!node) {  
            return;  
        }  
  
        *tree = node;  
        node->isLeaf = 0;  
        node->keynum = 0;  
        node->child[0] = root;  
  
        BTree_split_child(node, 0, root);  
  
        BTree_insert_nonfull(node, p_tcb, prio);  
    }  
  
    else {  
        BTree_insert_nonfull(root, p_tcb, prio);  
    }  
}


void BTree_merge_child(TwoThreeFourTree* tree, TwoThreeFourNode* node, CPU_INT32U index)  
{  
    OS_ERR err;
    CPU_INT32U i;  
    OS_TCB *p_tcb;
    CPU_INT32U prio;
    TwoThreeFourNode *leftChild, *rightChild;  

  
    prio = node->prio[index];
    p_tcb = node->p_tcb[index];
    
    leftChild = node->child[index];  
    rightChild = node->child[index + 1];  
    
   
    leftChild->prio[leftChild->keynum] = prio;
    leftChild->p_tcb[leftChild->keynum] = p_tcb;
    
    leftChild->child[leftChild->keynum + 1] = rightChild->child[0];  
    ++leftChild->keynum;  
  

    for (i = 0; i < rightChild->keynum; ++i) {  
        leftChild->prio[leftChild->keynum] = rightChild->prio[i];
        leftChild->p_tcb[leftChild->keynum] = rightChild->p_tcb[i];
        
        leftChild->child[leftChild->keynum + 1] = rightChild->child[i + 1];  
        ++leftChild->keynum;  
    }  
  

    for (i = index; i < node->keynum - 1; ++i) {  
        node->prio[i] = node->prio[i + 1];
        node->p_tcb[i] = node->p_tcb[i + 1];
        
        node->child[i + 1] = node->child[i + 2];  
    }  
    node->prio[node->keynum - 1] = 0;
    node->p_tcb[node->keynum - 1] = NULL;
    
    node->child[node->keynum] = NULL;  
    --node->keynum;  
  
  
    if (node->keynum == 0) {  
        if (*tree == node) {  
            *tree = leftChild;  
        }  
        OSMemPut((OS_MEM *)ReturnTwo34TreeBuffer(),(void *)node, &err);
        node = NULL;  
    }
    
    OSMemPut((OS_MEM *)ReturnTwo34TreeBuffer(),(void *)rightChild, &err);
    
    rightChild = NULL;  
}


void BTree_recursive_remove(TwoThreeFourTree* tree, OS_TCB *p_tcb, CPU_INT32U prio)  
{  
    OS_ERR err;
    CPU_INT32U i, j, index;  
    TwoThreeFourNode *root = *tree;  
    TwoThreeFourNode *node = root;  
  
    if (!root) {   
        return;  
    }  
  

    index = 0;  
    while (index < node->keynum && prio > node->prio[index]) {  
        ++index;  
    }  
  
    if (index < node->keynum && node->prio[index] == prio) {  
        TwoThreeFourNode *leftChild, *rightChild;  
        OS_TCB *left_tcb, *right_tcb;
        CPU_INT32U left_prio, right_prio;

        if (node->isLeaf == 1) {  
            for (i = index; i < node->keynum-1; ++i) {  
                node->prio[i] = node->prio[i + 1];
                node->p_tcb[i] = node->p_tcb[i + 1];
 
            }  
            node->prio[node->keynum-1] = 0;
            node->p_tcb[node->keynum-1] = NULL;

            --node->keynum;  
  
            if (node->keynum == 0) {
                //OSMemPut(&MemForTwoThreeFourTree,(void *)node, &err);
                *tree = NULL;  
            }  
  
            return;  
        }  

        else if (node->child[index]->keynum >= 2) {  
            leftChild = node->child[index];  
            left_prio = leftChild->prio[leftChild->keynum - 1];
            left_tcb = leftChild->p_tcb[leftChild->keynum - 1];
            node->prio[index] = left_prio;
            node->p_tcb[index] = left_tcb;
  
            BTree_recursive_remove(&leftChild, left_tcb, left_prio);  
        }  

        else if (node->child[index + 1]->keynum >= 2) {  
            rightChild = node->child[index + 1];  
            right_prio = rightChild->prio[0];
            right_tcb = rightChild->p_tcb[0];
            node->prio[index] = right_prio;
            node->p_tcb[index] = right_tcb;
  
            BTree_recursive_remove(&rightChild, right_tcb, right_prio);  
        }  
 
        else if (node->child[index]->keynum == 1  && node->child[index + 1]->keynum == 1){  
            leftChild = node->child[index];  
  
            BTree_merge_child(tree, node, index);  
   
            BTree_recursive_remove(&leftChild, p_tcb, prio);  
        }  
    }  
  

    else {  
        TwoThreeFourNode *leftSibling, *rightSibling, *child;  
 
        child = node->child[index];  
        if (!child) {  
            return;  
        }  

        if (child->keynum == 1) {  
            leftSibling = NULL;  
            rightSibling = NULL;  
  
            if (index - 1 >= 0) {  
                leftSibling = node->child[index - 1];  
            }  
  
            if (index + 1 <= node->keynum) {  
                rightSibling = node->child[index + 1];  
            }  
 
            if ((leftSibling && leftSibling->keynum >= 2) || (rightSibling && rightSibling->keynum >= 2)) {  
                CPU_INT32U richR = 0;  
                if(rightSibling) richR = 1;  
                if(leftSibling && rightSibling) {  
                    richR = cmp(rightSibling->keynum,leftSibling->keynum);  
                }  
                if (rightSibling && rightSibling->keynum >= 2 && richR) {  
  
                    child->prio[child->keynum] = node->prio[index];
                    child->p_tcb[child->keynum] = node->p_tcb[index];
                    
                    child->child[child->keynum + 1] = rightSibling->child[0];  
                    ++child->keynum;  
  
                    node->prio[index] = rightSibling->prio[0];  
                    node->p_tcb[index] = rightSibling->p_tcb[0]; 
  
                    for (j = 0; j < rightSibling->keynum - 1; ++j) { 
                        rightSibling->prio[j] = rightSibling->prio[j + 1];
                        rightSibling->p_tcb[j] = rightSibling->p_tcb[j + 1];  
                        
                        rightSibling->child[j] = rightSibling->child[j + 1];  
                    }  
                    rightSibling->prio[rightSibling->keynum-1] = 0;
                    rightSibling->p_tcb[rightSibling->keynum-1] = NULL; 
                    rightSibling->child[rightSibling->keynum-1] = rightSibling->child[rightSibling->keynum];  
                    rightSibling->child[rightSibling->keynum] = NULL;  
                    --rightSibling->keynum;  
                }  
                else {
                    for (j = child->keynum; j > 0; --j) {
                        child->prio[j] = child->prio[j - 1];
                        child->p_tcb[j] = child->p_tcb[j - 1];
                        
                        child->child[j + 1] = child->child[j];  
                    }  
                    child->child[1] = child->child[0];  
                    child->child[0] = leftSibling->child[leftSibling->keynum];  
                    child->prio[0] = node->prio[index - 1];  
                    child->p_tcb[0] = node->p_tcb[index - 1];
                    
                    ++child->keynum;  
  
                    node->prio[index - 1] = leftSibling->prio[leftSibling->keynum - 1];
                    node->p_tcb[index - 1] = leftSibling->p_tcb[leftSibling->keynum - 1];
  
                    leftSibling->prio[leftSibling->keynum - 1] = 0;
                    leftSibling->p_tcb[leftSibling->keynum - 1] = NULL;
                    
                    leftSibling->child[leftSibling->keynum] = NULL;  
  
                    --leftSibling->keynum;  
                }  
            }  

            else if ((!leftSibling || (leftSibling && leftSibling->keynum == 1))  
                && (!rightSibling || (rightSibling && rightSibling->keynum == 1))) {  
                if (leftSibling && leftSibling->keynum == 1) {  
  
                    BTree_merge_child(tree, node, index - 1);  
  
                    child = leftSibling;  
                }  
  
                else if (rightSibling && rightSibling->keynum == 1) {  
  
                    BTree_merge_child(tree, node, index); 
                }  
            }  
        }  
        BTree_recursive_remove(&child, p_tcb, prio); 
    }  
}  
  
void BTree_remove(TwoThreeFourTree* tree, OS_TCB *p_tcb, CPU_INT32U prio)  
{  
 
    if (*tree==NULL)  
    {      
        return;  
    }  
  
    BTree_recursive_remove(tree, p_tcb, prio);  
}