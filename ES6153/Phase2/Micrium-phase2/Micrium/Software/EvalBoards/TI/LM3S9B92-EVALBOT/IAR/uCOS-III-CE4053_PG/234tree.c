#include <stdio.h>
#include <stdlib.h>
#include "234tree.h"
int     t34TreeSize; //结点数量
t34_node* t34 (int i, t34_node* t) {
    t34_node N, *l, *r, *y;
    
    if (t == NULL)
        return t;
    
    N.left = N.right = NULL;
    l = r = &N;
    
    for (;;)
    {
        if (i < t->item)
        {
            if (t->left == NULL)
                break;
            if (i < t->left->item)
            {
                y = t->left;                           /* rotate right */
                t->left = y->right;
                y->right = t;
                t = y;
                if (t->left == NULL)
                    break;
            }
            r->left = t;                               /* link right */
            r = t;
            t = t->left;
        } else if (i > t->item)
        {
            if (t->right == NULL)
                break;
            if (i > t->right->item)
            {
                y = t->right;                          /* rotate left */
                t->right = y->left;
                y->left = t;
                t = y;
                if (t->right == NULL)
                    break;
            }
            l->right = t;                              /* link left */
            l = t;
            t = t->right;
        } else {
            break;
        }
    }
    l->right = t->left;                                /* assemble */
    r->left = t->right;
    t->left = N.right;
    t->right = N.left;
    return t;
}

/*
 **将i插入树t中,返回树的根结点(item值==i)
 */
t34_node* t34_insert(int i, t34_node *t,OS_TCB *tcb,int blocknumber,OS_MUTEX * mutex) {
    /* Insert i into the tree t, unless it's already there.    */
    /* Return a pointer to the resulting tree.                 */
    t34_node* node;
    OS_ERR err;
    node = (t34_node *) OSMemGet ((OS_MEM *)ReturnSlpayTreeBuffer(),
                                  &err);
    if (node == NULL){
        printf("Ran out of space\n");
        exit(1);
    }
    if(blocknumber==1){
      node->mutexsize=0;
    }
    node->item = i;
    node->tcb=tcb;
    node->blocknumber=blocknumber;
    node->mutex[node->mutexsize]=mutex;
    node->mutexsize++;
    if (t == NULL) {
        node->left = node->right = NULL;
        t34TreeSize = 1;
        return node;
    }
    t = t34(i,t);
    if (i < t->item) {  //令t为i的右子树
        node->left = t->left;
        node->right = t;
        t->left = NULL;
        t34TreeSize ++;
        return node;
    } else if (i > t->item) { //令t为i的左子树
        node->right = t->right;
        node->left = t;
        t->right = NULL;
        t34TreeSize++;
        return node;
    } else {
        OS_ERR err;
        OSMemPut((OS_MEM *)ReturnSlpayTreeBuffer(),
             node,
             &err);
        return t;
    }
}


/*
 **从树中删除i,返回树的根结点
 */
t34_node* t34_delete(int i, t34_node* t) {
    /* Deletes i from the tree if it's there.               */
    /* Return a pointer to the resulting tree.              */
    t34_node* x;
    if (t==NULL)
        return NULL;
    t = t34(i,t);
    if (i == t->item) {               /* found it */
        if (t->left == NULL) { //左子树为空,则x指向右子树即可
            x = t->right;
        } else {
            x = t34(i, t->left); //查找左子树中最大结点max,令右子树为max的右子树
            x->right = t->right;
        }
        t34TreeSize--;
        OS_ERR err;
        OSMemPut((OS_MEM *)ReturnSlpayTreeBuffer(),
             t,
             &err);
        return x;
    }
    return t;                         /* It wasn't there */
}

void t34_traverse(t34_node*    node,OS_TCB *tcb,t34_node **    Returnnode)
{
    if(node != NULL)
    {
        ST_inoder_traverse(node->left);
        if(node->tcb==tcb){
          *Returnnode=node;
            }
        printf("%d ", node->item);
        ST_inoder_traverse(node->right);
    }
}
        
void t34_post(t34_node*    node,int i,t34_node **    Returnnode)
{
    if(node != NULL)
    {
        ST_inoder_traverse(node->left);
        if(node->item==i){
           (*Returnnode)=node;
        }
        printf("%d ", node->item);
        ST_inoder_traverse(node->right);
    }
}
void t34_minimumunblock(t34_node*    node,t34_node **    Returnnode)
{
    if(node != NULL)
    {
        t34_minimumunblock(node->right,Returnnode);
        if(node->blocknumber==0){
           (*Returnnode)=node;
        }
        printf("%d ", node->item);
        t34_minimumunblock(node->left,Returnnode);
    }
} 

t34_node* t34_minimum(t34_node*    node){
  t34_node*    temp=node;
  while(temp->left!=NULL){
    temp=temp->left;
  }
  return temp;
}