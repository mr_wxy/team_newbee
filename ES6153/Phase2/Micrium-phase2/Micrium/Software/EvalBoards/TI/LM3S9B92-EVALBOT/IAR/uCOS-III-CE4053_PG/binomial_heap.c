

#include <stdio.h>
#include <stdlib.h>
#include "binomial_heap.h"

#define swap(a,b) (a^=b,b^=a,a^=b)


BinomialNode* binomial_search(BinomialHeap heap, Type key)
{
    BinomialNode *child;
    BinomialNode *parent = heap;

    parent = heap;
    while (parent != NULL)
    {
        if (parent->key == key)
            return parent;
        else
        {
            if((child = binomial_search(parent->child, key)) != NULL)
                return child;
            parent = parent->next;
        }
    }

    return NULL;
}


static void _binomial_minimum(BinomialHeap heap,
        BinomialNode **prev_y, BinomialNode **y)
{
    BinomialNode *x, *prev_x;    

    if (heap==NULL)
        return ;
 
    prev_x  = heap;
    x       = heap->next;
    *prev_y = NULL;
    *y      = heap;
 
    while (x != NULL) {
        if (x->key < (*y)->key) {
            *y = x;
            *prev_y = prev_x;
        }
        prev_x = x;
        x = x->next;
    }
}
 
BinomialNode* binomial_minimum(BinomialHeap heap) 
{
    BinomialNode *prev_y, *y;

    _binomial_minimum(heap, &prev_y, &y);
 
    return y;
}
 

static void binomial_link(BinomialHeap child, BinomialHeap heap) 
{
    child->parent = heap;
    child->next   = heap->child;
    heap->child = child;
    heap->degree++;
}
 

static BinomialNode* binomial_merge(BinomialHeap h1, BinomialHeap h2) 
{
    BinomialNode* head = NULL; 
    BinomialNode** pos = &head;

    while (h1 && h2)
    {
        if (h1->degree < h2->degree)
        {
            *pos = h1;
            h1 = h1->next;
        } 
        else 
        {
            *pos = h2;
            h2 = h2->next;
        }
        pos = &(*pos)->next;
    }
    if (h1)
        *pos = h1;
    else
        *pos = h2;

    return head;
}


BinomialNode* binomial_union(BinomialHeap h1, BinomialHeap h2) 
{
    BinomialNode *heap;
    BinomialNode *prev_x, *x, *next_x;


    heap = binomial_merge(h1, h2);
    if (heap == NULL)
        return NULL;
 
    prev_x = NULL;
    x      = heap;
    next_x = x->next;
 
    while (next_x != NULL)
    {
        if (   (x->degree != next_x->degree) 
            || ((next_x->next != NULL) && (next_x->degree == next_x->next->degree))) 
        {

            prev_x = x;
            x = next_x;
        } 
        else if (x->key <= next_x->key) 
        {

            x->next = next_x->next;
            binomial_link(next_x, x);
        } 
        else 
        {

            if (prev_x == NULL) 
            {
                heap = next_x;
            } 
            else 
            {
                prev_x->next = next_x;
            }
            binomial_link(x, next_x);
            x = next_x;
        }
        next_x = x->next;
    }

    return heap;
}


static BinomialNode* make_binomial_node(Type key,int period,OS_TCB *tcb)
{
    BinomialNode* node;
    

    OS_ERR err;
    node=(BinomialNode *)OSMemGet((OS_MEM *)ReturnBinomialBuffer(),
                                  &err);
    if (node==NULL)
    {
        printf("malloc BinomialNode failed!\n");
        return NULL;
    }
    node->tcb=tcb;
    node->period=period;
    node->key = key;
    node->degree = 0;
    node->parent = NULL;
    node->child = NULL;
    node->next = NULL;

    return node;
}  


BinomialNode* binomial_insert(BinomialHeap heap, Type key,int period,OS_TCB *tcb)
{
    BinomialNode* node;
    BinomialNode* node2;


    node = make_binomial_node(key,period,tcb);
    if (node==NULL)
        return heap;

    return binomial_union(heap, node);
} 


static BinomialNode* binomial_reverse(BinomialNode* heap)
{
    BinomialNode* next;
    BinomialNode* tail = NULL;

    if (!heap)
        return heap;

    heap->parent = NULL;
    while (heap->next) 
    {
        next          = heap->next;
        heap->next = tail;
        tail          = heap;
        heap          = next;
        heap->parent  = NULL;
    }
    heap->next = tail;

    return heap;
}

BinomialNode* binomial_extract_minimum(BinomialHeap heap)
{
    BinomialNode *y, *prev_y;    

    if (heap==NULL)
        return heap;
 

    _binomial_minimum(heap, &prev_y, &y);
 
    if (prev_y == NULL)    
        heap = heap->next;
    else                
        prev_y->next = y->next;
 

    BinomialNode* child = binomial_reverse(y->child);

    heap = binomial_union(heap, child);


    //free(y);
    OS_ERR err;
    OSMemPut((OS_MEM *)ReturnBinomialBuffer(),
             y,
             &err);

    return heap;
}


static void binomial_decrease_key(BinomialHeap heap, BinomialNode *node, Type key)
{
    if ((key >= node->key) || (binomial_search(heap, key) != NULL))
    {
        printf("decrease failed: the new key(%d) is existed already, \
                or is no smaller than current key(%d)\n", key, node->key);
        return ;
    }
    node->key = key;
 
    BinomialNode *child, *parent;
    child = node;
    parent = node->parent;
    while(parent != NULL && child->key < parent->key)
    {
        swap(parent->key, child->key);
        child = parent;
        parent = child->parent;
    }
}


static void binomial_increase_key(BinomialHeap heap, BinomialNode *node, Type key)
{
    if ((key <= node->key) || (binomial_search(heap, key) != NULL))
    {
        printf("increase failed: the new key(%d) is existed already, \
                or is no greater than current key(%d)\n", key, node->key);
        return ;
    }
    node->key = key;

    BinomialNode *cur, *child, *least;
    cur = node;
    child = cur->child;
    while (child != NULL) 
    {
        if(cur->key > child->key)
        {

            least = child;
            while(child->next != NULL)
            {
                if (least->key > child->next->key)
                {
                    least = child->next;
                }
                child = child->next;
            }

            swap(least->key, cur->key);


            cur = least;
            child = cur->child;
        }
        else
        {
            child = child->next;
        }
    }
}

 void binomial_update_key(BinomialHeap heap, BinomialNode* node, Type key)
{
    if (node == NULL)
        return ;

    if(key < node->key)
        binomial_decrease_key(heap, node, key);
    else if(key > node->key)
        binomial_increase_key(heap, node, key);
    else
        printf("No need to update!!!\n");
}
 

void binomial_update(BinomialHeap heap, Type oldkey, Type newkey)
{
    BinomialNode *node;

    node = binomial_search(heap, oldkey);
    if (node != NULL)
        binomial_update_key(heap, node, newkey);
}

BinomialNode* binomial_delete(BinomialHeap heap, Type key)
{
    BinomialNode *node;
    BinomialNode *parent, *prev, *pos;

    if (heap==NULL)
        return heap;


    if ((node = binomial_search(heap, key)) == NULL)
        return heap;


    parent = node->parent;
    while (parent != NULL)
    {
 
        swap(node->key, parent->key);
 
        node   = parent;
        parent = node->parent;
    }


    prev = NULL;
    pos  = heap;
    while (pos != node) 
    {
        prev = pos;
        pos  = pos->next;
    }

    if (prev)
        prev->next = node->next;
    else
        heap = node->next;

    heap = binomial_union(heap, binomial_reverse(node->child)); 

    //free(node);
    OS_ERR err;
    OSMemPut((OS_MEM *)ReturnBinomialBuffer(),
             node,
             &err);

    return heap;
}


static void _binomial_print(BinomialNode *node, BinomialNode *prev, int direction)
{
    while(node != NULL)
    {
        //printf("%2d \n", node->key);
        if (direction == 1)
            //printf("\t%2d(%d) is %2d's child\n", node->key, node->degree, prev->key);
            //modify the period - 300 s
            node->key=node->key-60;
        else
            //printf("\t%2d(%d) is %2d's next\n", node->key, node->degree, prev->key);
            //modify the period - 300 s
            node->key=node->key-60;
        if (node->child != NULL)
            _binomial_print(node->child, node, 1);


        prev = node;
        node = node->next;
        direction = 2;
    }
}
               
void binomial_print(BinomialHeap heap)
{
    if (heap == NULL)
        return ;

    BinomialNode *p = heap;
    //printf("== 二项堆( ");
    while (p != NULL) 
    {
        //printf("B%d ", p->degree);
        p = p->next;
    }
    //printf(")的详细信息：\n");

    int i=0;
    while (heap != NULL) 
    {
        i++;
        //printf("%d. 二项树B%d: \n", i, heap->degree);
        //printf("\t%2d(%d) is root\n", heap->key, heap->degree);
        //modify the period - 300 s
        heap->key=heap->key-60;
        _binomial_print(heap->child, heap, 1);
        heap = heap->next;
    }
    printf("\n");
}

