#include <stdio.h>
#include <stdlib.h>
#include <slpayTree.h>
int     slpayTreeSize; //结点数量
tree_node* splay (int i, tree_node* t) {
    tree_node N, *l, *r, *y;
    
    if (t == NULL)
        return t;
    
    N.left = N.right = NULL;
    l = r = &N;
    
    for (;;)
    {
        if (i <= t->item)
        {
            if (t->left == NULL)
                break;
            if (i <= t->left->item)
            {
                y = t->left;                           /* rotate right */
                t->left = y->right;
                y->right = t;
                t = y;
                if (t->left == NULL)
                    break;
            }
            r->left = t;                               /* link right */
            r = t;
            t = t->left;
        } else if (i > t->item)
        {
            if (t->right == NULL)
                break;
            if (i > t->right->item)
            {
                y = t->right;                          /* rotate left */
                t->right = y->left;
                y->left = t;
                t = y;
                if (t->right == NULL)
                    break;
            }
            l->right = t;                              /* link left */
            l = t;
            t = t->right;
        } else {
            break;
        }
    }
    l->right = t->left;                                /* assemble */
    r->left = t->right;
    t->left = N.right;
    t->right = N.left;
    return t;
}

/*
 **将i插入树t中,返回树的根结点(item值==i)
 */
tree_node* ST_insert(int i, tree_node *t,OS_TCB *tcb,OS_MUTEX    *p_mutex,int blocksize) {
    /* Insert i into the tree t, unless it's already there.    */
    /* Return a pointer to the resulting tree.                 */
    tree_node* node;
    OS_ERR err;
    node = (tree_node *) OSMemGet ((OS_MEM *)ReturnSlpayTreeBuffer(),
                                  &err);
    if (node == NULL){
        printf("Ran out of space\n");
        exit(1);
    }
    node->item = i;
    node->tcb=tcb;
    node->p_mutex=p_mutex;
    node->blocksize=blocksize;
    if (t == NULL) {
        node->left = node->right = NULL;
        slpayTreeSize = 1;
        return node;
    }
    t = splay(i,t);
    if (i <= t->item) {  //令t为i的右子树
        node->left = t->left;
        node->right = t;
        t->left = NULL;
        slpayTreeSize ++;
        return node;
    } else if (i > t->item) { //令t为i的左子树
        node->right = t->right;
        node->left = t;
        t->right = NULL;
        slpayTreeSize++;
        return node;
    } else {
        OS_ERR err;
        OSMemPut((OS_MEM *)ReturnSlpayTreeBuffer(),
             node,
             &err);
        return t;
    }
}


/*
 **从树中删除i,返回树的根结点
 */
tree_node* ST_delete(int i, tree_node* t) {
    /* Deletes i from the tree if it's there.               */
    /* Return a pointer to the resulting tree.              */
    tree_node* x;
    if (t==NULL)
        return NULL;
    t = splay(i,t);
    if (i == t->item) {               /* found it */
        if (t->left == NULL) { //左子树为空,则x指向右子树即可
            x = t->right;
        } else {
            x = splay(i, t->left); //查找左子树中最大结点max,令右子树为max的右子树
            x->right = t->right;
        }
        slpayTreeSize--;
        OS_ERR err;
        OSMemPut((OS_MEM *)ReturnSlpayTreeBuffer(),
             t,
             &err);
        return x;
    }
    return t;                         /* It wasn't there */
}

void ST_inoder_traverse(tree_node*    node,OS_MUTEX* p_mutex,tree_node **returnNode)
{
    if(node != NULL)
    {
        ST_inoder_traverse(node->left,p_mutex,returnNode);
        if(node->p_mutex==p_mutex){
          *returnNode=node;
        }
        printf("%d ", node->item);
        ST_inoder_traverse(node->right,p_mutex,returnNode);

    }
}

void ST_pre_traverse(tree_node*    node,OS_TCB *tcb,int *i)
{
    if(node != NULL)
    {
        if(node->tcb==tcb){
          *i=1;
        }
        //printf("%d ", node->item);
        ST_pre_traverse(node->left,tcb,i);
        ST_pre_traverse(node->right,tcb,i);
    }
}
void find_minimum(tree_node*    node,int *i){
    if(node != NULL)
    {
        find_minimum(node->left,i);
        if(node->tcb!=NULL){
          *i=1;
        }
        find_minimum(node->right,i);

    }
}
void find_highprio(tree_node*    node,OS_TCB **tcb){
    if(node != NULL)
    {
        find_highprio(node->right,tcb);
        if(node->tcb!=NULL){
          *tcb=node->tcb;
        }
        find_highprio(node->left,tcb);

    }
}

