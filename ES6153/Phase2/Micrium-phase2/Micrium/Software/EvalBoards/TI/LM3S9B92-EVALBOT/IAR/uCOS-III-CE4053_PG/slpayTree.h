#include <os.h>
#include <includes.h>



#define        NUM        20

typedef struct tree_node{
    int blocksize;
    int blocklist[10];
    OS_MUTEX    *p_mutex;
    OS_TCB *tcb;
    struct tree_node*    left;
    struct tree_node*    right;
    int        item;
}tree_node;
tree_node* ST_insert(int i, tree_node *t,OS_TCB *tcb,OS_MUTEX    *p_mutex,int blocksize);
tree_node* splay (int i, tree_node* t);
tree_node* ST_delete(int i, tree_node* t);
void ST_inoder_traverse(tree_node *    node,OS_MUTEX* p_mutex,tree_node **returnNode);
void ST_pre_traverse(tree_node*    node,OS_TCB *tcb,int *i);
void find_minimum(tree_node*    node,int *i);
void find_highprio(tree_node*    node,OS_TCB **tcb);