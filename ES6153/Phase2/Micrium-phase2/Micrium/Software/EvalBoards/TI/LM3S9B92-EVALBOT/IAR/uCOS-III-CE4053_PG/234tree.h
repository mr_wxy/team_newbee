#include <os.h>
#include <includes.h>

#define        NUM        20

typedef struct t34_node{
    int mutexsize;
    OS_MUTEX * mutex[10];
    int blocknumber;
    OS_TCB *tcb;
    struct t34_node*    left;
    struct t34_node*    right;
    int        item;
}t34_node;
t34_node* t34_insert(int i, t34_node *t,OS_TCB *tcb,int blocknumber,OS_MUTEX * mutex);
t34_node* t34 (int i, t34_node* t);
t34_node* t34_delete(int i, t34_node* t);
//void t34_traverse(t34_node*    node);
t34_node* t34_minimum(t34_node*    node);
void t34_traverse(t34_node*    node,OS_TCB *tcb,t34_node **    Returnnode);
void t34_post(t34_node*    node,int i,t34_node **    Returnnode);
void t34_minimumunblock(t34_node*    node,t34_node **    Returnnode);