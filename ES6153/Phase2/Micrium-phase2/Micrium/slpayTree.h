#include <os.h>
#include <includes.h>

typedef struct SplayNode *position;
typedef int ElementTP;

struct SplayNode {
    OS_TCB *tcb;
    position parent;
    ElementTP element;
    position lchild;
    position rchild;
};

/* pointer => root node of the tree */
typedef struct SplayNode *TREE;

TREE find_value(TREE, ElementTP);
position insert_value(TREE, ElementTP,OS_TCB *tcb);

static void splay_tree(TREE, position);
static position search_value(TREE, ElementTP);
static void with_grandpa(TREE, position);

static void insert_node_to_nonempty_tree(TREE, position);
static TREE left_single_rotate(TREE);
static TREE left_double_rotate(TREE);
static TREE right_single_rotate(TREE);
static TREE right_double_rotate(TREE);
static TREE left_zig_zig(TREE);
static TREE right_zig_zig(TREE);
SplayNode * find_minimum(TREE tr);
